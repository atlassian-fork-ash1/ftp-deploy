FROM alpine:3.9

RUN apk add --update --no-cache \
    bash=4.4.19-r1 \
    lftp=4.8.4-r1

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
